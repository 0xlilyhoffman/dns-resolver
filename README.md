# Iterative DNS Resolver

Project Demo: https://youtu.be/bbIvqbC-75U

For this project I used the DNS protocol to build an iterative DNS query resolver. The output mimics that of dig (domain information groper).

The program takes command line arguments of the host name to resolve, and an optional -m flag. If the flag is absent, the DNS resolver requests Type A records to resolve the host name IP address. If the flag is present, the DNS resolver requests Type MX records to find the mail exchange for the domain. 

The program uses root-servers.txt to begin the query. root-servers.txt contains a list of IP addresses for root DNS servers. The resolver then iteratively works its way down the DNS hierarchy, querying a root server, then a top-level domain server, and finally an authoritative server, where it can resolve the requested host name. 

If a server is queried and a response is not received in 10 seconds, the program moves on and queries the next server. 

The server prints out all responses as it traverses the DNS hierarchy. The output of each response mimics that of the dig tool. 