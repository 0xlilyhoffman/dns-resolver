#!/usr/bin/env python
#website for info on DNS message format:https://tools.ietf.org/html/rfc1035

import sys
import socket
from struct import *
import time
import string
import random


def main(argv=None):
    #Grab and interpret cmd line args    
    if argv is None:
        argv = sys.argv
    if "-m" in argv:
        qtype = 15
        hostname = argv[2] if ("=m" == argv[1]) else argv[1]
    else:
        qtype = 1
        hostname = argv[1]
    if validateHostname(hostname) == -1:
        print "Invalid hostname"
        sys.exit(0)   
    
    #Populate list of root server IP addresses from root-servers.txt file
    rootServers = open("root-servers.txt", 'r').read().splitlines() 
    queryServer(hostname, qtype,0, rootServers)

def validateHostname(hostname):
    if "." not in hostname:
        return -1
    if hostname[0] is "-":
        return -1 
    return 1


"""
Queries servers
"""
def queryServer(hostname, qtype, entry, rootServers):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(5)
    randomid = 24021
    found = False
    try:
        #Construct Query for hostname, send to root
        query = constructQuery(randomid, hostname, qtype)
        IP = rootServers[entry]
        sock.sendto(query,(IP, 53))
        response = sock.recv(4096)
        print "+--------------------------------------------------------------------------------------------+"
        print "\tQuerying ROOT SERVER at",  IP, "for host ", hostname
        print "+--------------------------------------------------------------------------------------------+"

        header, question, answer, authority, additional = decodeResponse(response, hostname)
        serverName,IP = findFirstIP(header, question, answer, authority, additional)
        
        ANCOUNT = header[10]
        while ANCOUNT == 0:
            query = constructQuery(randomid + random.randint(1, 100), hostname, qtype)
            sock.sendto(query, (IP, 53))
            print "+--------------------------------------------------------------------------------------------+"
            print "\tQuerying", serverName, " at",  IP, "for host ", hostname
            print "+--------------------------------------------------------------------------------------------+"

            response = sock.recv(4096)
            header, question, answer, authority, additional = decodeResponse(response, hostname)
            ANCOUNT = header[10]
            serverName,IP = findFirstIP(header, question, answer, authority, additional) 
            if serverName == "ERROR":
                break
        #ANSWER FOUND - PRINT RESULTS - EXIT
        finalResult = findAnswer(answer, header[10], authority, header[11], additional, header[12], hostname)
        if finalResult == "":
            sys.exit(0)     
        else:
            queryServer(finalResult, qtype, entry, rootServers)  
    except socket.timeout as e:
        print "Exception: ", e
        print "Trying next root server"
        entry +=1
        if entry == 12:
            print "Traced all root servers - could not find ", hostname
            sys.exit(0)
        queryServer(hostname, qtype, entry, rootServers)


def findAnswer(answer, ANCOUNT, authority, NSCOUNT, additional, ARCOUNT, hostname):
    #Find IP to print = find TYPE A in answer
    IP = "DEFAULT"
    for i in range(0, ANCOUNT, 1):
        if answer[i][2] == 1:
            IP = answer[i][6]
            break 
    
    #If TYPE MX and no TYPE A answers, print IP from authority/additional
    for i in range(0, NSCOUNT, 1):
        if authority[i][2] ==1:
            IP = authority[i][6] 
            break
    for i in range(0, ARCOUNT, 1):
        if additional[i][2] ==1:
            IP = additional[i][6] 
            break
    #IF no type A found, requery CNAME at root
    if IP == "DEFAULT":
        return answer[0][6]
        
    print "+-------------------------------------------------------------------------------------------+"
    print "+-------------------------------------------------------------------------------------------+"   
    print "+\t\t\t",  hostname, " @ ", IP
    print "+-------------------------------------------------------------------------------------------+"
    print "+-------------------------------------------------------------------------------------------+"
       
    return ""


#Called when answer count = 0
#Search AUTHORITY and ADDITIONAL responses for IP address
#Also, check for SOA=6
def findFirstIP(header, question, answer, authority, additional):
    NSCOUNT = header[11]
    ARCOUNT = header[12]
   
    #Search authority for A type - return first (servername, ip) found
    for i in range(0, NSCOUNT, 1):
        if authority[i][2] == 6:#SOA TYPE
            print "\n\n\nInvalid hostname\n\n\n"
            sys.exit(0)
        if authority[i][2] == 1: #A TYPE
            return authority[i][1], authority[i][6]
    #Search additional for A type - return first (servername, ip) found
    for i in range(0, ARCOUNT, 1):
        if additional[i][2] == 6: #SOA TYPE
            print "\n\n\nInvalid hostname\n\n\n"
            sys.exit(0)
        if additional[i][2] == 1: #A TYPE
            return additional[i][1], additional[i][6]    
    return "ERROR", "ERROR"

"""
Returns all data unpacked form DNS response
Returns: header, question, answer, authority, additional
    header =    (ID, QR, Opcode, AA, TR, RD, RA, Z, RCODE, QDCOUNT, ANCOUNT, NSCOUNT, ARCOUNT)
    question =  (QNAME, QTYPE, QCLASS)
    answer =    MAP{int->tuple} = id -> (response, NAME, TYPE, CLASS, TTL, RDLENGTH, RDATA)
    authority = MAP{int->tuple} = id -> (response, NAME, TYPE, CLASS, TTL, RDLENGTH, RDATA)
    additional= MAP{int->tuple} = id -> (response, NAME, TYPE, CLASS, TTL, RDLENGTH, RDATA)
"""
def decodeResponse(response, hostname):
    original_response = response
    """    
    +---------------------+
    |        Header       |
    +---------------------+
    |       Question      | the question for the name server
    +---------------------+
    |        Answer       | RRs answering the question
    +---------------------+
    |      Authority      | RRs pointing toward an authority
    +---------------------+
    |      Additional     | RRs holding additional information
    +---------------------+
    """

    #Note each  call modifies response - removes bytes read in
    #so that the next call can just read in "response" and start reading where the last one left off
    ######----------------------------------------- HEADER------------------------------------------------------"
    response, ID, QR, Opcode, AA, TR, RD, RA, Z, RCODE, QDCOUNT, ANCOUNT, NSCOUNT, ARCOUNT = decodeHeader(response)
    header = (ID, QR, Opcode, AA, TR, RD, RA, Z, RCODE, QDCOUNT, ANCOUNT, NSCOUNT, ARCOUNT)
    print "->>HEADER<<--  opcode: ", Opcode, "status:", RCODE, "id", ID
    print "QUERY: ", QDCOUNT, ", ANSWER: ", ANCOUNT, ", AUTHORITY: ", NSCOUNT, ", ADDITIONAL: ", ARCOUNT
    print "\n\n"

    ######----------------------------------------- QUESTION -------------------------------------------------- "
    print "QUESTION SECTION:"
    if QDCOUNT == 1:
        response, QNAME, QTYPE, QCLASS =  decodeQuestion(response, hostname, original_response)
    else:
        for count in range(0, QDCOUNT):
            response, QNAME, QTYPE, QCLASS =  decodeQuestion(response, hostname, original_response)
    question = (QNAME, QTYPE, QCLASS)
    TYPE_MAP = {1:"A", 2: "NS", 5: "CNAME", 6:"SOA", 11: "WKS", 12: "PTR", 15:"MX", 33:"SRC", 28:"AAAA"}
    CLASS_MAP = {1:"IN"}
    print QNAME, "\t\t", (CLASS_MAP[QCLASS] if QCLASS in CLASS_MAP else QCLASS),"\t", (TYPE_MAP[QTYPE] if QTYPE in TYPE_MAP else QTYPE)

    #######------------------------------------------- ANSWER -------------------------------------------------- "
    updated_response = response
    print "\nANSWER SECTION:"
    answer = {}
    if ANCOUNT != 0:
        answer = decodeAnswer(response, hostname, ANCOUNT, original_response)
        updated_response = answer[ANCOUNT-1][0]
        printRR(answer, ANCOUNT)
   #######------------------------------------------ AUTHORITY------------------------------------------------- "
    print "\nAUTHORITY SECTION: "
    authority = {}
    if NSCOUNT != 0:
        authority = decodeAuthority(updated_response, hostname, NSCOUNT, original_response)
        printRR(authority, NSCOUNT)
        updated_response = authority[NSCOUNT-1][0]    
    #######------------------------------------------ ADDITIONAL ----------------------------------------------- "
    print "\nADDITIONAL SECTION: "
    additional = {}
    if ARCOUNT != 0:
        additional = decodeAdditional(updated_response, hostname, ARCOUNT, original_response)
        printRR(additional, ARCOUNT)
    return header, question, answer, authority, additional
    
def printRR(answer, count):
    TYPE_MAP = {1:"A", 2: "NS", 5: "CNAME", 6:"SOA", 11: "WKS", 12: "PTR", 15:"MX", 33:"SRC", 28:"AAAA"}
    CLASS_MAP = {1: "IN", 2: "?"} 
    for count in range(0, count):
        print answer[count][1], "\t\t\t", answer[count][4], "\t",CLASS_MAP.get(answer[count][3]),"\t",  TYPE_MAP.get(answer[count][2]), "\t",  answer[count][6]
    print "\n\n"

def decodeHeader(response):
    """Unpack the byte code response from the DNS server"""
    """
    The header contains the following fields:

                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      ID                       |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    QDCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ANCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    NSCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ARCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+


    ID              A 16 bit identifier assigned by the program that
                    generates any kind of query.  This identifier is copied
                    the corresponding reply and can be used by the requester
                    to match up replies to outstanding queries.

    QR              A one bit field that specifies whether this message is a
                    query (0), or a response (1).

    OPCODE          A four bit field that specifies kind of query in this
                    message.  This value is set by the originator of a query
                    and copied into the response.  The values are:
                        0               a standard query (QUERY)
                        1               an inverse query (IQUERY)
                        2               a server status request (STATUS)
                        3-15            reserved for future use

    AA              Authoritative Answer - this bit is valid in responses,
                    and specifies that the responding name server is an
                    authority for the domain name in question section.
                    Note that the contents of the answer section may have
                    multiple owner names because of aliases.  The AA bit
                    corresponds to the name which matches the query name, or
                    the first owner name in the answer section.

    TC              TrunCation - specifies that this message was truncated
                    due to length greater than that permitted on the
                    transmission channel.

    RD              Recursion Desired - this bit may be set in a query and
                    is copied into the response.  If RD is set, it directs
                    the name server to pursue the query recursively.
                    Recursive query support is optional.

    RA              Recursion Available - this be is set or cleared in a
                    response, and denotes whether recursive query support is
                    available in the name server.

    Z               Reserved for future use.  Must be zero in all queries
                    and responses.

    RCODE           Response code - this 4 bit field is set as part of
                    responses. 
                    0               No error condition
                    1               Format error - The name server was
                                    unable to interpret the query.
                    2               Server failure - The name server was
                                    unable to process this query due to a
                                    problem with the name server.
    e               3               Name Error - Meaningful only for
                                    responses from an authoritative name
                                    server, this code signifies that the
                                    domain name referenced in the query does
                                    not exist.
                    4               Not Implemented - The name server does
                                    not support the requested kind of query.
                    5               Refused - The name server refuses to
                                    perform the specified operation for
                                    policy reasons.  For example, a name
                                    server may not wish to provide the
                                    information to the particular requester,
                                    or a name server may not wish to perform
                                    a particular operation (e.g., zone
                                    transfer) for particular data.
                    6-15            Reserved for future use.

    QDCOUNT         an unsigned 16 bit integer specifying the number of
                    entries in the question section.

    ANCOUNT         an unsigned 16 bit integer specifying the number of
                    resource records in the answer section.

    NSCOUNT         an unsigned 16 bit integer specifying the number of name
                    server resource records in the authority records
                    section.

    ARCOUNT         an unsigned 16 bit integer specifying the number of
                    resource records in the additional records section.
    """
 
    #see https://docs.python.org/2/library/struct.html?highlight=unpack#struct.unpack for format codes  
    start = 0
    end = start+calcsize('H')
    ID = unpack('!H', response[start:end])[0]
    
    start = end
    end = start+calcsize('H')
    flags = unpack('!H', response[start:end])[0]   
    flags = bin(int(flags))
    flags = flags[2:]
    flags = "0"*(16-len(flags)) + flags

    QR = flags[0]
    QR_MAP = {'0': "QUERY",'1': "RESPONSE"}
    Opcode = flags[1:5]
    Opcode_MAP = {'0000': "QUERY", '0001': "IQUERY", '0010': "STATUS"} 
    
    AA = flags[5]
    AA_MAP = {'1': "A",'0': " "}
    TR = flags[6]
    TR_MAP = {'1': "TC", '0':" " }
    RD = flags[7]
    RD_MAP = {'1': "RD", '0': " "}  
    RA = flags[8]
    RA_MAP = {'1': "RA", '0': " "} 
    Z = flags[9:12]
    RCODE = flags[12:16]
    RCODE_MAP = {'0000': "NOERROR", '0001': "Format error", '0010': "Server failure", '0011': "Name Error"}

    start = end
    end = start+calcsize('H')
    QDCOUNT = unpack('!H', response[start:end])[0]
   
    start = end
    end = start+calcsize('H')
    ANCOUNT = unpack('!H', response[start:end])[0]
    
    start = end
    end = start + calcsize('H')
    NSCOUNT = unpack('!H', response[start:end])[0]
    
    start = end
    end = start + calcsize('H')
    ARCOUNT = unpack('!H', response[start:end])[0]

    return response[end:], ID, QR, Opcode_MAP.get(Opcode), AA_MAP[AA], TR_MAP[TR], RD_MAP[RD], RA_MAP[RA], Z, RCODE_MAP.get(RCODE), QDCOUNT, ANCOUNT, NSCOUNT, ARCOUNT

def decodeQuestion(response, hostname, original_response):
    """
                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                                               |
    /                     QNAME                     /
    /                                               /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                     QTYPE                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                     QCLASS                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+


    QNAME           a domain name represented as a sequence of labels, where
                    each label consists of a length octet followed by that
                    number of octets.  The domain name terminates with the
                    zero length octet for the null label of the root.  Note
                    that this field may be an odd number of octets; no
                    padding is used.

    QTYPE           a two octet code which specifies the type of the query.
                    The values for this field include all codes valid for a
                    TYPE field, together with some more general codes which
                    can match more than one type of RR.
                    Requests __ record for the domain name
                        1 - A 
                        2 - NS
                        3 - CNAME
                        6 - SOA
                        @see http://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml for full list
                     
    QCLASS          a two octet code that specifies the class of the query.
                    For example, the QCLASS field is IN for the Internet.
                        1 - IN
                        @see http://www.iana.org/assignments/dns-parameters/dns-parameters.xhtml for full list

    """
    QNAME = networkToString(response, 0)[0]
    end = networkToString(response, 0)[1]

    start = end
    end = start + calcsize('H')
    QTYPE = unpack('!H', response[start:end])[0]

    start = end
    end = start+calcsize('H')
    QCLASS = unpack('!H', response[start:end])[0]
   
    return response[end:], QNAME, QTYPE, QCLASS

def decodeResourceRecord(response, hostname, original_response):
    """
    The answer, authority, and additional sections all share the same
    format: a variable number of resource records, where the number of
    records is specified in the corresponding count field in the header.
    Each resource record has the following format:
                                        1  1  1  1  1  1
          0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
        +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        |                                               |
        /                                               /
        /                      NAME                     /
        |                                               |
        +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        |                      TYPE                     |
        +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        |                     CLASS                     |
        +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        |                      TTL                      |
        |                                               |
        +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        |                   RDLENGTH                    |
        +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--|
        /                     RDATA                     /
        /                                               /
        +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

    NAME            a domain name to which this resource record pertains.

    TYPE            two octets containing one of the RR type codes.  This
                    field specifies the meaning of the data in the RDATA
                    field.
                        Meaning/Use
                        x'0001 (1)  An A record for the domain name
                        x'0002 (2)  A NS record( for the domain name
                        x'0005 (5)  A CNAME record for the domain name
                        x'0006 (6)  A SOA record for the domain name
                        x'000B (11) A WKS record(s) for the domain name
                        x'000C (12) A PTR record(s) for the domain name
                        x'000F (15) A MX record for the domain name
                        x'0021 (33) A SRV record(s) for the domain name
                        x'001C (28) An AAAA record(s) for the domain name

    CLASS           two octets which specify the class of the data in the
                    RDATA field.

    TTL             a 32 bit unsigned integer that specifies the time
                    interval (in seconds) that the resource record may be
                    cached before it should be discarded.  Zero values are
                    interpreted to mean that the RR can only be used for the
                    transaction in progress, and should not be cached.

    RDLENGTH        an unsigned 16 bit integer that specifies the length in
                    octets of the RDATA field.

    RDATA           a variable length string of octets that describes the
                    resource.  The format of this information varies
                    according to the TYPE and CLASS of the resource record.
                    For example, the if the TYPE is A and the CLASS is IN,
                    the RDATA field is a 4 octet ARPA Internet address.
    """

    start = 0
    end = start + calcsize('H')
    namefield = unpack('!H', response[start:end])[0]
    namefield_int = int(namefield)
    namefield_binary = format(namefield_int, '#018b')
    name_or_pointer = namefield_binary[2:4]
    if name_or_pointer == "11":   
        name_offset = namefield_binary[4:]    
        name_offset = int(name_offset, 2)
        NAME = networkToString(original_response, name_offset)[0]
    else:
        NAME = networkToString(response, 0)[0]
    
    start = end
    end = start + calcsize('H')
    TYPE = unpack('!H', response[start:end])[0]
    TYPE_MAP = {1:"A", 2: "NS", 5: "CNAME", 6:"SOA", 11: "WKS", 12: "PTR", 15:"MX", 33:"SRC", 28:"AAAA"}
 
    start = end
    end = start + calcsize('H')
    CLASS = unpack('!H', response[start:end])[0]
    CLASS_MAP = {1: "IN"}    
 
    start = end
    end = start + calcsize('I')
    TTL = unpack('!I', response[start:end])[0]

    start = end 
    end = start + calcsize('H')
    RDLENGTH = unpack('!H', response[start:end])[0]

    start = end
    end = start + RDLENGTH
    RDATA = ""
    if TYPE == 1:#A
        typelength = 4 
    if TYPE == 28:#AAAA
        typelength = 16
    if TYPE != 1 and TYPE != 28:
        typelength = 0
    for i in range(0, typelength, 1):
        RDATA_i =  unpack('B', response[start+i:start+(i+1)])[0]
        RDATA += str(RDATA_i) + "."
    RDATA =RDATA[:-1]
 
    if TYPE ==2:
        RDATA = RDATA

    if TYPE ==5:
        RDATA_pt1 = networkToString2(response, start)[0]
        pointer_at_end = networkToString2(response, start)[2]
        if pointer_at_end == 1:
            last_two_bytes = unpack('!H', response[end-2:end])[0]
            last_two_int = int(last_two_bytes)      
            last_two_binary = format(last_two_int, '#018b') 
            offset = last_two_binary[4:]
            offset = int(offset, 2)
            RDATA_pt2 = networkToString(original_response, offset)[0]
            RDATA = RDATA_pt1+ "." +  RDATA_pt2
        else:
            RDATA = RDATA_pt1 


    if TYPE == 15:
        offset = 2 #2 bytes of RDATA are "preference" bits:
        RDATA_pt1 = networkToString2(response, start+offset)[0]
        pointer_at_end = networkToString2(response, start+offset)[2]
        if pointer_at_end == 1:
            last_two_bytes = unpack('!H', response[end-2:end])[0]
            last_two_int = int(last_two_bytes)      
            last_two_binary = format(last_two_int, '#018b') 
            offset = last_two_binary[4:]
            offset = int(offset, 2)
            RDATA_pt2 = networkToString(original_response, offset)[0]
            RDATA = RDATA_pt1+ "." +  RDATA_pt2
        else:
            RDATA = RDATA_pt1 

    return response[end:], NAME,  TYPE, CLASS, TTL, RDLENGTH, RDATA 


def decodeAnswer(response, hostname, ANCOUNT, original_response):
    answer = {}
    for count in range(0, ANCOUNT, 1):
        response, NAME, TYPE, CLASS, TTL, RDLENGTH, RDATA =  decodeResourceRecord(response, hostname, original_response)
        answer[count] = (response, NAME, TYPE, CLASS, TTL, RDLENGTH, RDATA)
    return answer

def decodeAuthority(response, hostname, NSCOUNT, original_response):
    authority= {}
    for count in range(0, NSCOUNT, 1):
        response, NAME, TYPE, CLASS, TTL, RDLENGTH, RDATA =  decodeResourceRecord(response, hostname, original_response)
        authority[count] = (response, NAME, TYPE, CLASS, TTL, RDLENGTH, RDATA)
    return authority

def decodeAdditional(response, hostname, ARCOUNT, original_response):
    additional = {}
    for count in range(0, ARCOUNT, 1):
        response, NAME, TYPE, CLASS, TTL, RDLENGTH, RDATA =  decodeResourceRecord(response, hostname, original_response)
        additional[count] = (response, NAME, TYPE, CLASS, TTL, RDLENGTH, RDATA)
    return additional

def stringToNetwork(orig_string):
    """
    Converts a standard string to a string that can be sent over
    the network.

    Args:
        orig_string (string): the string to convert

    Returns:
        string: The network formatted string.

    Example:
        stringToNetwork('www.sandiego.edu.edu') will return
          (3)www(8)sandiego(3)edu(0)
    """
    ls = orig_string.split('.')
    toReturn = ""
    for item in ls:
        formatString = "B"
        formatString += str(len(item))
        formatString += "s"
        toReturn += pack(formatString, len(item), item)
    toReturn += pack("B", 0)
    return toReturn




def networkToString2(response, start):
    """
    Converts a network response string into a human readable string.

    Args:
        response (string): the entire network response message
        start (int): the location within the message where the network string
            starts.

    Returns:
        string: The human readable string.

    Example:  networkToString('(3)www(8)sandiego(3)edu(0)', 0) will return
              'www.sandiego.edu'
    """

    toReturn = ""
    position = start
    length = -1
    while True:
        length = unpack("!B", response[position])[0]
        if length == 0 or length == 192:
            position += 1
            if length == 192:
                pointer = 1
            if length == 0:
                pointer = 0
            break

        # Handle DNS pointers (!!)
        elif (length & 1 << 7) and (length & 1 << 6):
            b2 = unpack("!B", response[position+1])[0]
            offset = 0
            for i in range(6) :
                offset += (length & 1 << i)
            for i in range(8):
                offset += (b2 & 1 << i)
            dereferenced = networkToString(response, offset)[0]
            return toReturn + dereferenced, position + 2

        formatString = str(length) + "s"
        position += 1
        toReturn += unpack(formatString, response[position:position+length])[0]
        toReturn += "."
        position += length
    return toReturn[:-1], position, pointer
    


def networkToString(response, start):
    """
    Converts a network response string into a human readable string.

    Args:
        response (string): the entire network response message
        start (int): the location within the message where the network string
            starts.

    Returns:
        string: The human readable string.

    Example:  networkToString('(3)www(8)sandiego(3)edu(0)', 0) will return
              'www.sandiego.edu'
    """

    toReturn = ""
    position = start
    length = -1
    while True:
        length = unpack("!B", response[position])[0]
        if length == 0:
            position += 1
            break

        # Handle DNS pointers (!!)
        elif (length & 1 << 7) and (length & 1 << 6):
            b2 = unpack("!B", response[position+1])[0]
            offset = 0
            for i in range(6) :
                offset += (length & 1 << i)
            for i in range(8):
                offset += (b2 & 1 << i)
            dereferenced = networkToString(response, offset)[0]
            return toReturn + dereferenced, position + 2

        formatString = str(length) + "s"
        position += 1
        toReturn += unpack(formatString, response[position:position+length])[0]
        toReturn += "."
        position += length
    return toReturn[:-1], position
    

def constructQuery(ID, hostname, QTYPE):
    """
    Constructs a DNS query message for a given hostname and ID.

    Args:
        ID (int): ID # for the message
        hostname (string): What we're asking for

    Returns: 
        string: "Packed" string containing a valid DNS query message
    """
    flags = 0 # 0 implies basic iterative query

    # one question, no answers for basic query
    num_questions = 1
    num_answers = 0
    num_auth = 0
    num_other = 0

    # "!HHHHHH" means pack 6 Half integers (i.e. 16-bit values) into a single
    # string, with data placed in network order (!)
    header = pack("!HHHHHH", ID, flags, num_questions, num_answers, num_auth,
            num_other)

    qname = stringToNetwork(hostname)
    qtype = QTYPE # request A type (MX type = 15)
    remainder = pack("!HH", qtype, 1)
    query = header + qname + remainder
    return query

if __name__ == "__main__":
    sys.exit(main())




